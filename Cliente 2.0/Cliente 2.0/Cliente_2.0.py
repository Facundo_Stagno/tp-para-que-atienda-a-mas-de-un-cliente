import socket

ClientSocket = socket.socket()
host = input("ingresar el host(localhost): ")
port = int(input("ingresar el puerto(8050): "))

print('Conectando...')
try:
    ClientSocket.connect((host, port))
except socket.error as e:
    print(str(e))

Response = ClientSocket.recv(1024)
while True:
    Input = input('Diga algo señor, si quiere salir del programa escriba SALIR: ')
    ClientSocket.send(str.encode(Input))
    Response = ClientSocket.recv(1024)
    print(Response.decode('utf-8'))
    if(Input=="SALIR"):
        break

ClientSocket.close()
