import socket
import os
from _thread import *

ServerSocket = socket.socket()
host = 'localhost'
port = 8050
ThreadCount = 0
try:
    ServerSocket.bind((host, port))
except socket.error as e:
    print(str(e))

print('Conectandose..')
ServerSocket.listen(5)


def threaded_client(connection):
    connection.send(str.encode('Buenos dias'))
    while True:
        data = connection.recv(2048)
        reply = 'Server Says: *SONIDO DE RADIO* Rogerthat *SONIDO DE RADIO*' 
        if not data:
            break
        connection.sendall(str.encode(reply))
    connection.close()

while True:
    Client, address = ServerSocket.accept()
    print('Connected to: ' + address[0] + ':' + str(address[1]))
    start_new_thread(threaded_client, (Client, ))
    ThreadCount += 1
    print('Thread Number: ' + str(ThreadCount))
ServerSocket.close()